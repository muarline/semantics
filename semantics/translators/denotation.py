import re

spc = '( )'
bln = '('+spc+'*'+')'
ns = '('+'(\n)'+'|'+spc+')'

var = '([a-z]+[a-z0-9]*)'
const = '([0-9]+)'
const_or_var = '('+var+'|'+const+')'
operation = '([-+*])'
relation = '([<=>]|(~=)|(>=)|(<=))'

expression = '('+const_or_var+'|'+'('+const_or_var+bln+operation+bln+const_or_var+')'+')'

assignment = '('+var+bln+':='+bln+expression+')'
comparison = '('+const_or_var+bln+relation+bln+const_or_var+')'

regex_assignment = '^('+bln+assignment+bln+')$'

regex_if = '^('+bln+'[(]'+bln+'IF'+ns+spc+comparison+spc+ns+'THEN'+ns+bln+'(?P<sub1>`|[^`]*)'+bln+ns+'ELSE'+ns+bln+'(?P<sub2>`|[^`]*)'+bln+ns+'[)]'+bln+')$'
regex_while = '^('+bln+'[(]'+bln+'WHILE'+ns+spc+comparison+spc+ns+'DO'+ns+bln+'(?P<sub>`|[^`]*)'+bln+ns+'[)]'+bln+')$'
regex_two_parts = [
    '^('+bln+'[(]'+bln+'(?P<sub1>'+assignment+')'+bln+ns+';'+ns+bln+'(?P<sub2>[(].*[)])'+bln+'[)]'+bln+')$',
    '^('+bln+'[(]'+bln+'(?P<sub1>`|'+assignment+')'+bln+ns+';'+ns+bln+'(?P<sub2>`|'+assignment+')'+bln+'[)]'+bln+')$',
    '^('+bln+'[(]'+bln+'(?P<sub1>[(].*[)])'+bln+ns+';'+ns+bln+'(?P<sub2>[(].*[)])'+bln+'[)]'+bln+')$',
    '^('+bln+'[(]'+bln+'(?P<sub1>[(].*[)])'+bln+ns+';'+ns+bln+'(?P<sub2>'+assignment+')'+bln+'[)]'+bln+')$',
]

regex = {
    'two_parts': regex_two_parts,
    'if': [regex_if],
    'while': [regex_while],
    'assignment': [regex_assignment],
}

constr = {
    'two_parts': '[[;]]',
    'if': '[[if-then...else...]]',
    'while': '[[while-do...]]',
}

mapping = {
    'two_parts': '{}+{}',
    'if': 'min({},{})',
    'while': 0,
    'assignment': 1,
}

count = {
    'two_parts': lambda x: sum([int(a) for a in x]),
    'if': lambda x: min([int(a) for a in x]),
    'while': lambda x: 0,
    'assignment': lambda x: 1,
}


class Program():

    def __init__(self, text):
        self.text = text
        self.type = None
        self.subprograms = []
        self.value = None
        self.lvl = 0

    def __str__(self):
        if self.lvl == 0:
            return self.text
        if self.lvl == 1:
            return '[[{}]]'.format(self.text)
        if self.lvl == 2:
            return constr[self.type]+'('+','.join([str(p) for p in self.subprograms])+')'
        if self.lvl == 3:
            return mapping[self.type].format(*self.subprograms)
        if self.lvl == 4:
            return str(count[self.type]([str(p) for p in self.subprograms]))

    def cut_parts(self):
        p = re.compile('^('+bln+'(?P<text>.*)'+bln+')$', re.DOTALL)
        text = p.match(self.text).group('text')
        upd_text = ['(']
        sub_text = []
        sub = []
        counter = 0
        start, finish = 0, 0

        for i in range(1, len(text)-1):
            if text[i] == '(':
                counter += 1
                if counter == 1:
                    start = i
            if text[i] == ')':
                counter += -1
                if counter == 0:
                    finish = i
                    sub_text.append(text[start:finish+1])
                    upd_text.append('`')

            if counter == 0 and text[i] != ')':
                upd_text.append(text[i])

        upd_text.append(')')
        text = ''.join(upd_text)

        if self.type == 'if':
            p = re.compile(regex_if, re.DOTALL)
            match = p.match(text)
            if match is None:
                raise(Exception(self.text))
            sub1 = match.group('sub1')
            sub2 = match.group('sub2')

            if sub1[0] == '`':
                sub.append(Program(sub_text[0]))
                sub_text = sub_text[1:]
            else:
                sub.append(Program(sub1))

            if sub2[0] == '`':
                sub.append(Program(sub_text[0]))
            else:
                sub.append(Program(sub2))

        if self.type == 'while':
            p = re.compile(regex_while, re.DOTALL)
            match = p.match(text)
            if match is None:
                raise(Exception(self.text))
            sub1 = match.group('sub')

            if sub1[0] == '`':
                sub.append(Program(sub_text[0]))
            else:
                sub.append(Program(sub1))

        if self.type == 'two_parts':
            p = re.compile(regex_two_parts[1], re.DOTALL)
            match = p.match(text)
            if match is None:
                raise(Exception(self.text))
            sub1 = match.group('sub1')
            sub2 = match.group('sub2')

            if sub1[0] == '`':
                sub.append(Program(sub_text[0]))
                sub_text = sub_text[1:]
            else:
                sub.append(Program(sub1))

            if sub2[0] == '`':
                sub.append(Program(sub_text[0]))
            else:
                sub.append(Program(sub2))

        for p in sub:
            self.subprograms.append(p)

    def set_type(self):
        # Check basic types - if, while, assignment
        keys = ['if', 'while', 'assignment', 'two_parts']
        for type in keys:
            for reg in regex[type]:
                p = re.compile(reg, re.DOTALL)
                match = p.match(self.text)
                if match is not None:
                    self.type = type
                    if self.type != 'assignment':
                        self.cut_parts()
        if self.type is None:
            raise(Exception(self.text))
        for i in self.subprograms:
            i.set_type()

    def count(self):
        result = []
        while not self.lvl == 4:
            result.append(str(self))
            self.lvl_up()
        result.append(str(self))
        return result

    def lvl_up(self):

        # check if assignment
        if self.type == 'assignment' and self.lvl == 1:
            self.lvl = 4
            return
        if self.type == 'while' and self.lvl == 2:
            self.lvl = 4
            return

        # counting lvl
        if self.lvl == 3:
            can_lvl_up = True
            for p in self.subprograms:
                can_lvl_up = can_lvl_up and p.lvl == 4
            self.lvl = 4 if can_lvl_up else 3

        # update lvl
        if self.lvl > 0:
            for p in self.subprograms:
                p.lvl_up()

        if self.lvl < 3:
            self.lvl += 1


def translate(text):
    text = text.replace('\r', '')
    p = Program(text)
    p.set_type()
    return p.count()
