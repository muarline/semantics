import re

bln = '( *)'
ns = '([ \n])'
var = '([a-z][0-9a-z]*)'
cv = '([0-9]+|' + var + ')'
op = '([*+-])'
relation = '((>=)|(<=)|(~=)|[=><])'
comp = '(' + cv + bln + relation + bln + cv + ')'
exp = '(' + cv + '(' + bln + op + bln + cv + ')?)'
assignment = '(' + bln + var + bln + ':=' + bln + exp + bln + ')'
ASSIGN = '^(' + bln + var + bln + ':=' + bln + exp + bln + ')$'
IF = '^(' + bln + '[(]' + bln + 'IF' + ns + ' ' + comp + ' ' + ns + 'THEN' + ns + '(?P<pr1>`|[^`]*)' + ns + 'ELSE' + ns + '(?P<pr2>`|[^`]*)' + ns + '[)]' + bln + ')$'
WHILE = '^(' + bln + '[(]' + bln + 'WHILE' + ns + ' ' + comp + ' ' + ns + 'DO' + ns + '(?P<pr>`|[^`]*)' + ns + '[)]' + bln + ')$'
SEQAA = '^(' + bln + '[(]' + bln + '(?P<pr1>`|' + assignment + ')' + ns + ';' + ns + '(?P<pr2>`|' + assignment + ')' + bln + '[)]' + bln + ')$'
SEQAP = '^(' + bln + '[(]' + bln + '(?P<pr1>' + assignment + ')' + ns + ';' + ns + '(?P<pr2>[(](.*)[)])' + bln + '[)]' + bln + ')$'
SEQPA = '^(' + bln + '[(]' + bln + '(?P<pr1>[(](.*)[)])' + ns + ';' + ns + '(?P<pr2>' + assignment + ')' + bln + '[)]' + bln + ')$'
SEQPP = '^(' + bln + '[(]' + bln + '(?P<pr1>[(](.*)[)])' + ns + ';' + ns + '(?P<pr2>[(](.*)[)])' + bln + '[)]' + bln + ')$'

patterns = {
    'assign': [ASSIGN],
    'if': [IF],
    'while': [WHILE],
    'sequence': [SEQAA, SEQAP, SEQPA, SEQPP]
}


def is_correct(program):
    det = separate(program)

    if (det[0] == 'assign'):
        return True
    if (det[0] == 'while'):
        return is_correct(det[1][0])
    if (det[0] == 'if'):
        return is_correct(det[1][0]) and is_correct(det[1][1])
    if (det[0] == 'sequence'):
        return is_correct(det[1][0]) and is_correct(det[1][1])


def separate(program):
    key = None
    for i in patterns:
        for j in patterns[i]:
            to_check = re.compile(j, re.DOTALL)
            match = to_check.match(program)
            if (match != None):
                key = i
    # here Ok
    if key == None:
        raise (Exception('Invalid syntaxin program: {}!'.format(program)))
    if key == 'assign':
        return ['assign']

    # remove spaces
    flag = True
    start = 0
    end = len(program) - 1
    while start < len(program) and flag:
        if program[start] != ' ':
            flag = False
        else:
            start += 1
    flag = True

    while end >= 0 and flag:
        if program[end] != ' ':
            flag = False
        else:
            end -= 1
    program = program[start: end + 1]
    #
    proc_program = ['(']
    outer = 0
    sub = []
    subs = []
    for i in program[1:len(program) - 1]:
        if i == "(":
            outer += 1
        if i == ")":
            outer -= 1
            if outer == 0:
                sub.append(i)
                subs.append(''.join(sub.copy()))
                sub = []
                proc_program.append('`')
        if outer == 0 and i != ')':
            proc_program.append(i)
        if outer > 0:
            sub.append(i)
    proc_program.append(')')
    args = []
    proc_program = ''.join(proc_program)

    if key == 'while':
        reg = re.match(WHILE, proc_program)
        if reg == None:
            raise (Exception('Invalid syntaxin program: {}!'.format(program)))
        if reg.group('pr') == '`':
            args = [subs[0]]
        else:
            args = [reg.group('pr')]
        if is_correct(args[0]):
            return ('while', args)
        else:
            raise (Exception('Invalid syntaxin program: {}!'.format(args[0])))

    if key == 'if':
        reg = re.match(IF, proc_program)
        if reg == None:
            raise (Exception('Invalid syntaxin program: {}!'.format(program)))

        if reg.group('pr1') == '`':
            args.append(subs[0])
            if reg.group('pr2') == '`':
                args.append(subs[1])
            else:
                args.append(reg.group('pr2'))
        else:
            args.append(reg.group('pr1'))
            if reg.group('pr2') == '`':
                args.append(subs[0])
            else:
                args.append(reg.group('pr2'))
        return ('if', args)

    if key == 'sequence':
        reg = re.match(SEQAA, proc_program)
        if reg == None:
            raise (Exception('Invalid syntaxin program: {}!'.format(program)))

        if reg.group('pr1') == '`':
            args.append(subs[0])
            if reg.group('pr2') == '`':
                args.append(subs[1])
            else:
                args.append(reg.group('pr2'))
        else:
            args.append(reg.group('pr1'))
            if reg.group('pr2') == '`':
                args.append(subs[0])
            else:
                args.append(reg.group('pr2'))
        return ('sequence', args)


class Wp():

    def __init__(self, program, num):
        self.program = program
        self.num = num

    def __str__(self):
        st = 'wp({},{})'.format(str(self.program), str(self.num))
        return st

    def counter(self):
        if (type(self.num) != int):
            self.num = self.num.counter()
        else:
            det = separate(self.program)
            if (det[0] == 'assign'):
                return self.num - 1
            if (det[0] == 'while'):
                return self.num
            if (det[0] == 'if'):
                return MaxOfTwo(Wp(det[1][0], self.num), Wp(det[1][1], self.num))
            if (det[0] == 'sequence'):
                return Wp(det[1][0], Wp(det[1][1], self.num))
        return self


class MaxOfTwo():
    def __init__(self, arg1, arg2):
        self.arg1 = arg1
        self.arg2 = arg2

    def __str__(self):
        st = 'max({},{})'.format(str(self.arg1), str(self.arg2))
        return st

    def counter(self):
        if (type(self.arg1) != int):  # arg2 should be changed
            self.arg1 = self.arg1.counter()
        elif (type(self.arg2) != int):
            self.arg2 = self.arg2.counter()
        else:
            return max(self.arg1, self.arg2)
        return self


def process(root):
    result = []
    while (type(root) != int):
        root = root.counter()
        result.append(str(root))
    return result


def translate(tel_code, n):
    tel_code = tel_code.replace('\r', '')
    root = Wp(tel_code, int(n))
    return process(root)
