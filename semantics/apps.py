from django.apps import AppConfig


class SemanticsConfig(AppConfig):
    name = 'semantics'
