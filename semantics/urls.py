from django.urls import path
from .views import denotation, weakest_precondition, index
from django.conf import settings
from django.conf.urls.static import static

app_name = 'semantics'
urlpatterns = [
    path('', index, name='index'),
    path('denotation/', denotation, name='denotation'),
    path('weakest_precondition/', weakest_precondition, name='weakest_precondition'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
