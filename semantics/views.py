from django.shortcuts import render
from .translators.denotation import translate as denotation_translate
from .translators.weakest_precondition import translate as weakest_precondition_translate
from .forms import DenotationalForm, WeakestPreconditionForm


def index(request):
    return render(request, 'semantics/index.html')


def denotation(request):
    data = {}
    if request.method == 'POST':
        form = DenotationalForm(request.POST)
        if form.is_valid():
            code = form.cleaned_data['code']
            data['code'] = code
            with open('log.txt', 'w') as file:
                file.write(code)
            try:
                steps = denotation_translate(code)
            except Exception as e:
                data['is_mistake'] = True
                data['mistake'] = str(e)
            else:
                data['is_mistake'] = False
                data['steps'] = steps
    else:
        form = DenotationalForm()

    return render(request, 'semantics/denotation.html', {'data': data})


def weakest_precondition(request):
    data = {}
    if request.method == 'POST':
        form = WeakestPreconditionForm(request.POST)

        if form.is_valid():
            code = form.cleaned_data['code']
            data['code'] = code
            number = form.cleaned_data['number']
            data['number'] = number
            with open('log.txt', 'w') as file:
                file.write(code)
            try:
                steps = weakest_precondition_translate(code, number)
            except Exception as e:
                data['is_mistake'] = True
                data['mistake'] = str(e)
            else:
                data['is_mistake'] = False
                data['steps'] = steps
    else:
        form = WeakestPreconditionForm()

    return render(request, 'semantics/weakest_precondition.html', {'data': data})
