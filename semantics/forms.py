from django import forms


class DenotationalForm(forms.Form):
    code = forms.CharField()


class WeakestPreconditionForm(forms.Form):
    code = forms.CharField()
    number = forms.IntegerField()
